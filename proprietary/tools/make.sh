#!/bin/sh

wget -nc -q https://dl.google.com/dl/android/aosp/nakasig-jdq39-factory-0798439d.tgz
tar zxf nakasig-jdq39-factory-0798439d.tgz
cd nakasig-jdq39
unzip image-nakasig-jdq39.zip
cd ../
./simg2img nakasig-jdq39/system.img system.ext4.img
mkdir system
sudo mount -o loop -t ext4 system.ext4.img system
sync

./make-sub-1.sh
./make-sub-2.sh

sudo umount system
rmdir system
rm -rf tmp
rm -rf nakasig-jdq39
rm system.ext4.img

